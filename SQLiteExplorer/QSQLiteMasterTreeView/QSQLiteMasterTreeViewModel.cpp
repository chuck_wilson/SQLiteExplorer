#include "QSQLiteMasterTreeViewModel.h"

QSQLiteMasterTreeViewModel::QSQLiteMasterTreeViewModel(QObject *parent)
    : QAbstractItemModel(parent)
{
}

QVariant QSQLiteMasterTreeViewModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section)
    Q_UNUSED(orientation)
    Q_UNUSED(role)
    // FIXME: Implement me!
    return QVariant();
}

bool QSQLiteMasterTreeViewModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (value != headerData(section, orientation, role)) {
        // FIXME: Implement me!
        emit headerDataChanged(orientation, section, section);
        return true;
    }
    return false;
}

QModelIndex QSQLiteMasterTreeViewModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(row)
    Q_UNUSED(column)
    Q_UNUSED(parent)
    // FIXME: Implement me!
    return QModelIndex();
}

QModelIndex QSQLiteMasterTreeViewModel::parent(const QModelIndex &index) const
{
    Q_UNUSED(index)
    // FIXME: Implement me!
    return QModelIndex();
}

int QSQLiteMasterTreeViewModel::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid())
        return 0;

    // FIXME: Implement me!
    return 0;
}

int QSQLiteMasterTreeViewModel::columnCount(const QModelIndex &parent) const
{
    if (!parent.isValid())
        return 0;

    // FIXME: Implement me!
    return 0;
}

bool QSQLiteMasterTreeViewModel::hasChildren(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    // FIXME: Implement me!
    return false;
}

bool QSQLiteMasterTreeViewModel::canFetchMore(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    // FIXME: Implement me!
    return false;
}

void QSQLiteMasterTreeViewModel::fetchMore(const QModelIndex &parent)
{
    Q_UNUSED(parent)
    // FIXME: Implement me!
}

QVariant QSQLiteMasterTreeViewModel::data(const QModelIndex &index, int role) const
{
    Q_UNUSED(role)
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    return QVariant();
}

bool QSQLiteMasterTreeViewModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags QSQLiteMasterTreeViewModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

bool QSQLiteMasterTreeViewModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endInsertRows();
    return true;
}

bool QSQLiteMasterTreeViewModel::insertColumns(int column, int count, const QModelIndex &parent)
{
    beginInsertColumns(parent, column, column + count - 1);
    // FIXME: Implement me!
    endInsertColumns();
    return true;
}

bool QSQLiteMasterTreeViewModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endRemoveRows();
    return true;
}

bool QSQLiteMasterTreeViewModel::removeColumns(int column, int count, const QModelIndex &parent)
{
    beginRemoveColumns(parent, column, column + count - 1);
    // FIXME: Implement me!
    endRemoveColumns();
    return true;
}
