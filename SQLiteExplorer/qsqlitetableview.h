#ifndef QSQLITETABLEVIEW_H
#define QSQLITETABLEVIEW_H

#include <QWidget>
#include <QTableView>
#include <QTableWidget>

#include "SQLite3DB.h"
class MainWindow;

struct SQLiteAuthInfo
{
    int nCode;
    QString strCode;
    QString s1;
    QString s2;
    QString s3;
    QString s4;
};

class QSQLiteTableView : public QTableWidget
{
    Q_OBJECT
public:
    QSQLiteTableView(QWidget *parent = 0);

    CSQLite3DB* SetDb(CSQLite3DB* pDb);

signals:
    void dataLoaded(const QString& msg);
    void authTriggerd(int nCode, QString strCode,
                      QString s1, QString s2, QString s3, QString s4);

public slots:
    // sql语句接收到
    void onSQLiteQueryReceived(const QString& sql);

    // sql语句认证触发，等到语句执行成功，再发出authTriggerd信号，给到mainWindow去更新左侧数据库表列表
    void onAuthTriggerd(int nCode, QString strCode,
                       QString s1, QString s2, QString s3, QString s4);

    void onValueChanged(int value);

private:
    //MainWindow* m_pParent;
    CSQLite3DB* m_pCurSQLite3DB;
    CppSQLite3Query m_curQuery;
    int m_rowThresh;

    QVector<SQLiteAuthInfo> m_authInfos;
};

#endif // QSQLITETABLEVIEW_H
