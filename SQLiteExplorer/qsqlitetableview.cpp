#include "QSQLiteTableView.h"
#include "mainwindow.h"
#include <QMessageBox>
#include <QHeaderView>
#include <QDebug>

QSQLiteTableView::QSQLiteTableView(QWidget *parent)
: QTableWidget(parent)
, m_pCurSQLite3DB(nullptr)
, m_rowThresh(100)
{
//    MainWindow* pMainWindow = qobject_cast<MainWindow*>(parent);
//    if (pMainWindow)
//    {
//        m_pParent = pMainWindow;
//    }

    QHeaderView *headers = horizontalHeader();
    //SortIndicator为水平标题栏文字旁边的三角指示器
    headers->setSortIndicator(0, Qt::AscendingOrder);
    headers->setSortIndicatorShown(true);
    connect(this->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(onValueChanged(int)));
}

CSQLite3DB *QSQLiteTableView::SetDb(CSQLite3DB *pDb)
{
    m_pCurSQLite3DB = pDb;
    disconnect(m_pCurSQLite3DB, SIGNAL(AuthTriggerd(int,QString,QString,QString,QString,QString)),
            this, SLOT(onAuthTriggerd(int,QString,QString,QString,QString,QString)));
    connect(m_pCurSQLite3DB, SIGNAL(AuthTriggerd(int,QString,QString,QString,QString,QString)),
            this, SLOT(onAuthTriggerd(int,QString,QString,QString,QString,QString)));
    return m_pCurSQLite3DB;
}

void QSQLiteTableView::onSQLiteQueryReceived(const QString &sql)
{
    if (!m_pCurSQLite3DB)
    {
        return;
    }

    m_authInfos.clear();

    clear();
    setColumnCount(0);
    setRowCount(0);
    m_rowThresh = 100;

    try
    {
        string s = sql.toStdString();
        m_curQuery = m_pCurSQLite3DB->execQuery(s.c_str());
        CppSQLite3Query& q = m_curQuery;
        QStringList headers;
        for(int i=0; i<q.numFields(); i++)
        {
            headers.push_back(QString::fromStdString(q.fieldName(i)));
        }
        setColumnCount(headers.size());

        setHorizontalHeaderLabels(headers);

        while (!q.eof())
        {
            insertRow(rowCount());
            for(int col=0; col<q.numFields(); col++)
            {
                QTableWidgetItem *name = new QTableWidgetItem();
                name->setText(QString::fromStdString(q.getStringField(col)));
                setItem(rowCount()-1, col, name);
            }
            q.nextRow();
            if(rowCount() >= m_rowThresh)
            {
                break;
            }
        }

        QString msg;
        if(!q.eof())
        {
            msg = QString("数据过多，已加载%1条记录").arg(rowCount());
        }
        else
        {
            msg = QString("数据加载完成，共加载%1条记录").arg(rowCount());
        }
        emit dataLoaded(msg);

        for(int i=0; i<m_authInfos.size(); i++)
        {
            SQLiteAuthInfo info = m_authInfos[i];
            emit authTriggerd(info.nCode, info.strCode, info.s1, info.s2, info.s3, info.s4);
        }
    }
    catch(CppSQLite3Exception& e)
    {
        QMessageBox::information(this, tr("SQLiteExplorer"), QString::fromStdString(e.errorMessage()));
    }

    resizeColumnsToContents();
    resizeRowsToContents();
}

void QSQLiteTableView::onAuthTriggerd(int nCode, QString strCode, QString s1, QString s2, QString s3, QString s4)
{
    SQLiteAuthInfo info;
    info.nCode = nCode;
    info.strCode = strCode;
    info.s1 = s1;
    info.s2 = s2;
    info.s3 = s3;
    info.s4 = s4;

    m_authInfos.push_back(info);
}

void QSQLiteTableView::onValueChanged(int value)
{
//    qDebug() << "value =" << value << ", VSBar Max =" << verticalScrollBar()->maximum()
//             << ", m_rowThresh =" << m_rowThresh;
    if(value == verticalScrollBar()->maximum() && !m_curQuery.eof())
    {
        //qDebug() << "Enter ";
        m_rowThresh *= 2;
        CppSQLite3Query& q = m_curQuery;
        while (!q.eof())
        {
            insertRow(rowCount());
            for(int col=0; col<q.numFields(); col++)
            {
                QTableWidgetItem *name = new QTableWidgetItem();
                name->setText(QString::fromStdString(q.getStringField(col)));
                setItem(rowCount()-1, col, name);
            }
            q.nextRow();
            if(rowCount() >= m_rowThresh)
            {
                break;
            }
        }
        QString msg;
        if(!q.eof())
        {
            msg = QString("数据过多，已加载%1条记录").arg(rowCount());
        }
        else
        {
            msg = QString("数据加载完成，共加载%1条记录").arg(rowCount());
        }
        emit dataLoaded(msg);
        //qDebug() << msg;
    }
}
